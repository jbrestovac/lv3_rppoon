﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cetvrti_zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationManager testManager = new NotificationManager();
            ConsoleColor currentForeground = Console.ForegroundColor;
            DateTime time = new DateTime();
            time = DateTime.Now;

            ConsoleNotification firstConsoleNotification = new ConsoleNotification("Jelena", "Brestovac", "First notification", time, Category.ERROR, currentForeground);
            testManager.Display(firstConsoleNotification);
            Console.WriteLine();

            ConsoleNotification secondConsoleNotification = new ConsoleNotification("Jelena", "Brestovac", "Second notification", time, Category.ALERT, currentForeground);
            testManager.Display(secondConsoleNotification);
            Console.WriteLine();

            ConsoleNotification thirdConsoleNotification = new ConsoleNotification("Jelena", "Brestovac", "Third notification", time, Category.INFO, currentForeground);
            testManager.Display(thirdConsoleNotification);
            Console.WriteLine();
        }
    }
}


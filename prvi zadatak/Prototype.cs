﻿using System;
using System.Collections.Generic;
using System.Text;

namespace prvi_zadatak
{
    interface Prototype
    {
        Prototype Clone();
    }
}

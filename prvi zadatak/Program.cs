﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace prvi_zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("C:\\Users\\Jelena\\source\\repos\\RPPOON-LV3\\File.csv"); // file is in project root, need to go out from root/bin/Delease or root/bin/Debug
            Console.WriteLine("Data:");
            dataset.DisplayData();

            Dataset datasetShallow = (Dataset)dataset.Clone();
            Console.WriteLine("Shallow data:");
            dataset.DisplayData();

            Dataset datasetDeep = (Dataset)dataset.CloneDeep();
            Console.WriteLine("Deep data:");
            datasetDeep.DisplayData();
        }
    }
}

﻿using System;

namespace drugi_zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            MatrixGenerator testMatrixGenerator = MatrixGenerator.GetInstance();

            double[][] testMatrix = testMatrixGenerator.CreateRandomMatrix(5, 4);

            for (int i = 0; i < testMatrix.Length; i++)
            {
                Console.Write(i+ ": ");

                for (int j = 0; j < testMatrix[i].Length; j++)
                {
                    Console.Write(testMatrix[i][j]);
                }
                Console.WriteLine();
            }
        }
    }
}

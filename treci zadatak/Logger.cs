﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace treci_zadatak
{
    class Logger
    {
        private static Logger instance;
        private string filePath;
        private Logger()
        {
            this.filePath = "C:\\Users\\Jelena\\source\\repos\\RPPOON-LV3\\File1.csv";
        }

        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();

            }
            return instance;
        }
        public void Set(string filePath)
        {
            this.filePath = filePath;
        }
        public void Log(string message)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath, true))
            {
                writer.WriteLine(message);
            }
        }
    }

}


